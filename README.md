# RaceAssist

## Commands

### Audience

/raceassist audience join [RaceID]  観客に自分を追加します<br>
/raceassist audience leave [RaceID] 観客から自分を削除します<br>
/raceassist audience list [RaceID]  観客の一覧を表示<br>

### Place

/raceassist place reverse [RaceID]  レースの走行方向の向きを反転<br>
/raceassist place central [RaceID]  レースの中心点を設定<br>
/raceassist place degree [RaceID]  レースのゴールの角度を設定(立っている場所基準90度刻み)<br>
/raceassist place lap [RaceID] <lap>  レースのラップ数を指定<br>
/raceassist place set [RaceID] in|out レース場の内周、外周を指定<br>
/raceassist place finish 上記の設定の終了<br>

### Player

/raceassist player add [RaceID] [Player]  騎手を追加<br>
/raceassist player remove [RaceID]  騎手を削除<br>
/raceassist player delete [RaceID]  騎手をすべて削除<br>
/raceassist player list [RaceID]  騎手の一覧を表示<br>

### Race

/raceassist race start [RaceID]  レースを開始<br>
/raceassist race debug [RaceID]  レースのデバッグ<br>
/raceassist race stop [RaceID]  レースの停止<br>
/raceassist race create [RaceID]  レースの作成<br>
/raceassist race delete [RaceID]  レースの削除<br>
